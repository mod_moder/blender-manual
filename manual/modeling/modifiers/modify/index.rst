
########
  Edit
########

.. toctree::
   :maxdepth: 1

   data_transfer.rst
   mesh_cache.rst
   mesh_sequence_cache.rst
   normal_edit.rst
   uv_project.rst
   uv_warp.rst
   weight_edit.rst
   weight_mix.rst
   weight_proximity.rst
   weighted_normal.rst
