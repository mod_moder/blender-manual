
##################
  Geometry Nodes
##################

Nodes that can operate on different geometry types (volume, mesh).

.. toctree::
   :maxdepth: 2

   Read <read/index.rst>
   Sample <sample/index.rst>
   Write <write/index.rst>

-----

.. toctree::
   :maxdepth: 2

   Operations <operations/index.rst>

-----

.. toctree::
   :maxdepth: 1

   geometry_to_instance.rst
   join_geometry.rst
