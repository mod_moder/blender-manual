.. _tool-grease-pencil-weight-paint-weight:

**************
Brush Settings
**************

Painting needs paint brushes and Blender provides a Brush panel within the Toolbar
when in *Weight Paint Mode*.

Brush
   In the :ref:`Data-Block menu <ui-data-block>` you find predefined Brush presets.
   And you can create your own custom presets as needed.

Radius :kbd:`F`
   The radius defines the area of influence of the brush.
Strength
   This is the amount of paint to be applied per brush stroke.
Use Falloff
   When enabled, use Strength falloff for the brush.
   Brush Strength decays with the distance from the center of the brush.
Weight :kbd:`Ctrl-F`
   The weight (visualized as a color) to be used by the brush.

   Using :kbd:`Ctrl-RMB` you can set the weight to the value thats under the cursor.
Direction :kbd:`D`
   Brush direction toggle, *Add* adds weight value while *Subtract* removes weight value.
   This setting can be toggled with :kbd:`D`.
