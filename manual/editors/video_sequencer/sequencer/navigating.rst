
**********
Navigating
**********

Header
======

.. figure:: /images/video-editing_sequencer_navigating_header.png
   :align: center

   Video Sequencer Header.


.. _bpy.types.SpaceSequenceEditor.show_region_hud:

View Menu
---------

As usual, the View menu controls the editor's view settings.

Sidebar :kbd:`N`
   Show or hide the :ref:`Sidebar <ui-region-sidebar>`.
Tool Settings
   Show or hide the settings for the currently selected tool.
Toolbar :kbd:`T`
   Show or hide the :ref:`Toolbar <ui-region-toolbar>`.
Adjust Last Operation
   See :ref:`bpy.ops.screen.redo_last`.
Channels
   Show or hide the :ref:`bpy.types.SequenceTimelineChannel`.
Preview as Backdrop
   Display the current frame in the background.

Frame Selected :kbd:`NumpadPeriod`
   Zooms the display to show only the selected strips.
Frame All :kbd:`Home`
   Zooms the display to show all strips.
Go to Current Frame
   Centers the horizontal timeline on the current frame.
Zoom :kbd:`Shift-B`
   Click and drag to draw a rectangle and zoom to this rectangle.

.. _bpy.types.SpaceSequenceEditor.use_clamp_view:

Limit View to Contents
   Prevents you from panning higher than the highest used channel.

.. _bpy.ops.sequencer.refresh_all:

Refresh All :kbd:`Ctrl-R`
   Reloads external files, re-renders the 3D Viewport, and refreshes the current frame preview.

   This is useful when you modified an external file or made a change in a scene that Blender
   didn't detect.

Navigation
   Play Animation :kbd:`Spacebar`
      Start or stop animation playback. This will start playback in all editors.
   Go to Current Frame :kbd:`Numpad0`
      Scrolls the timeline so the current frame is in the center.
   Jump to Previous Strip :kbd:`PageDown`
      Moves the playhead to the nearest strip border (start or end) that's before the current frame.
   Jump to Next Strip :kbd:`PageUp`
      Moves the playhead to the nearest strip border (start or end) that's after the current frame.
   Jump to Previous Strip (Center) :kbd:`Alt-PageDown`
      Moves the playhead to the nearest strip center that's before the current frame.
   Jump to Next Strip (Center) :kbd:`Alt-PageUp`
      Moves the playhead to the nearest strip center that's after the current frame.

.. _bpy.ops.sequencer.previewrange_set:

Range
   Set Preview Range :kbd:`P`
      Interactively define the frame range used for preview playback/rendering.

      As long as this range is active, playback will be limited to it, letting you repeatedly view a
      segment of the video without having to manually rewind each time. It also limits the range
      that gets rendered.
   Set Preview Range to Strips
      Apply a preview range that encompasses the selected strips.
   Clear Preview Range :kbd:`Alt-P`
      Clears the preview range.
   Set Start Frame :kbd:`Ctrl-Home`
      Set the Start frame of the scene to the current frame.
   Set End Frame :kbd:`Ctrl-End`
      Set the End frame of the scene to the current frame.
   Set Frame Range to Strips
      Set the Start and End frames of the scene so they encompass the selected strips.

Sync Visible Range
   Synchronizes the visible time range with other time-based editors (such as the Timeline and
   the Graph Editor) that also have this option enabled.

Show Seconds :kbd:`Ctrl-T`
   Shows seconds instead of frames on the time axis.
Show Markers
   Shows the marker region. When disabled, the *Marker* menu is also hidden
   and marker operators are not available in this editor.

.. _bpy.types.SequenceEditor.show_cache:

Show Cache
   Show which frames are :doc:`Cached </editors/video_sequencer/sequencer/sidebar/cache>`.

   The :ref:`Developer Extras <prefs-interface-dev-extras>` must be enabled for this
   menu item to be visible.

Sequence Render Image
   Show the current frame preview as a Render Result where you can save it as an image file.
Sequence Render Animation
   Save previews of the frames in the scene range (or the preview range, if active) to a video file
   or a series of image files. See the :doc:`/render/output/properties/output` panel for details.

.. note::
   *Sequence Render Image* and *Sequence Render Animation* don't render the final video by default --
   specifically, they don't render Scene Strips, instead using the preview's
   :doc:`shading mode </editors/3dview/display/shading>` (which is initially Solid).

   To output a video where the Scene Strips are rendered, use the *Render* menu in the topbar,
   or change :menuselection:`Sidebar --> View --> Scene Strip Display --> Shading` to *Rendered*.
   The latter option is only available if the Video Sequencer is in the *Preview* or
   *Sequencer & Preview* mode.

Export Subtitles
   Exports :doc:`Text strips </video_editing/edit/montage/strips/text>`,
   which can act as subtitles, to a `SubRip <https://en.wikipedia.org/wiki/SubRip>`__ file (``.srt``).
   The exported file contains all Text strips in the video sequence.

Toggle Sequencer/Preview :kbd:`Ctrl-Tab`
   Switch the editor mode between *Sequencer* and *Preview*.


Markers Menu
------------

:doc:`Markers </animation/markers>` are used to denote frames with key points or significant events
within an animation. Like with most animation editors, markers are shown at the bottom of the editor.

.. figure:: /images/editors_graph-editor_introduction_markers.png

   Markers in animation editor.

See :ref:`Editing Markers <animation-markers-editing>` for details.


Main View
=========

Adjusting the View
------------------

Use these shortcuts to adjust the view:

- Pan: :kbd:`MMB`
- Horizontal scroll: use :kbd:`Ctrl-Wheel`, or drag the horizontal scrollbar.
- Vertical scroll: use :kbd:`Shift-Wheel`, or drag the vertical scrollbar.
- Zoom: :kbd:`Wheel`
- Scale view: :kbd:`Ctrl-MMB` and drag left/right (horizontal scale) or up/down (vertical scale).
  Alternatively, you can drag the circles on the scrollbars with :kbd:`LMB`.

Playhead
--------

The Playhead is the blue vertical line with the current frame number at the top. It can be moved
in the following ways:

* Jump or scrub: click or drag :kbd:`LMB` in the scrubbing area at the top of the timeline.
* Jump or scrub (alternative): click or drag :kbd:`Shift-RMB` anywhere in the timeline.
  If you start dragging on a strip, that strip will be highlighted and displayed *solo* in the preview
  (all other strips are temporarily muted).
* Move in single-frame increments: :kbd:`Left`, :kbd:`Right`, or :kbd:`Alt-Wheel`.
* Jump to the start or end frame of the scene (or :ref:`preview range <bpy.ops.sequencer.previewrange_set>`,
  if active): :kbd:`Shift-Left` or :kbd:`Shift-Right`.

While dragging with :kbd:`LMB` or :kbd:`Shift-RMB`, you can additionally hold :kbd:`Ctrl`
to snap to the start and end points of strips.

If scrubbing (or regular playback) performs poorly, you can speed it up by creating
:doc:`proxies </editors/video_sequencer/sequencer/sidebar/proxy>`.

.. hint::

   The current frame is synchronized across all editors, so if you move the Playhead in the
   Timeline editor for example, it will move in the Video Sequence editor as well (and vice versa).